/*
 * SystemTask.cpp
 *
 *  Created on: 14/nov/2014
 *      Author: Simone
 */

#include "SystemTask.hpp"

using namespace MalpenEngine;

SystemTask::SystemTask( void* data, unsigned int priority )
	: Task( priority )
{
}

SystemTask::~SystemTask()
{
}
