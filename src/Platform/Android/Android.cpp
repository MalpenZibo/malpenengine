/*
 * Init.cpp
 *
 *  Created on: 14/nov/2014
 *      Author: Simone
 */

#ifndef SRC_PLATFORM_ANDROID_INIT_CPP_
#define SRC_PLATFORM_ANDROID_INIT_CPP_

#include "android_native_app_glue.h"
#include "../../Core/Engine.hpp"
#include "../../Enum/LogSeverity.hpp"
#include "AndroidTask.hpp"
#include <unistd.h>

static android_app* mAppState;

void android_main( struct android_app* state )
{
	mAppState = state;

	MalpenEngine::Engine engine( state );

	if( engine.Initialize() )
	{
		engine.Run();
	}
}

#endif /* SRC_PLATFORM_ANDROID_INIT_CPP_ */
