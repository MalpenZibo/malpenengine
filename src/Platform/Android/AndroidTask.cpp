/*
 * AndroidTask.cpp
 *
 *  Created on: 14/nov/2014
 *      Author: Simone
 */

#include "AndroidTask.hpp"
#include "../../Core/Engine.hpp"
#include "../../Log.hpp"

using namespace MalpenEngine;

AndroidTask::AndroidTask( void* data, const unsigned int priority, Engine* pEngine )
	: SystemTask( data, priority )
{
	mPState = (android_app*)data;
	mPState->onAppCmd = android_handle_cmd;
	mPState->userData = static_cast<void*>( pEngine );
}

AndroidTask::~AndroidTask()
{
}

bool AndroidTask::Start()
{
	return true;
}

void AndroidTask::OnSuspend()
{
}

void AndroidTask::Update()
{
	int events;
	struct android_poll_source* pSource;
	int ident = ALooper_pollAll( 0, 0, &events, (void**)&pSource );
	if( ident >= 0 )
	{
		if( pSource )
		{
			pSource->process( mPState, pSource );
		}

		if( mPState->destroyRequested )
		{
			Log::Info( "Destruction requested" );
			//mClosing = true;
		}
	}
}

void AndroidTask::OnResume()
{
}

void AndroidTask::Stop()
{
}

void AndroidTask::android_handle_cmd( struct android_app* app, int32_t cmd )
{
	Engine* pEngine;

	switch( cmd )
	{
		case APP_CMD_INIT_WINDOW:
			Log::Info( "Application Windows Initialization" );
			pEngine = static_cast<Engine*>( app->userData );
			pEngine->StartRender();

			//Renderer* pRenderer = static_cast<Renderer*>( app->userData );
			//assert( pRenderer );
			//pRenderer->Init();

			break;

		case APP_CMD_DESTROY:
			Log::Info( "Request Application Destruction" );
			pEngine = static_cast<Engine*>( app->userData );
			pEngine->Stop();


			//Renderer* pRenderer = static_cast<Renderer*>( app->userData );
			//assert( pRenderer );
			//pRenderer->Destroy();

			break;

		case APP_CMD_TERM_WINDOW:
			Log::Info( "Request Application Window termination" );
			pEngine = static_cast<Engine*>( app->userData );
			pEngine->StopRender();

			//Renderer* pRenderer = static_cast<Renderer*>( app->userData );
			//assert( pRenderer );
			//pRenderer->Destroy();

			break;

		case APP_CMD_RESUME:
			pEngine->SetPaused( false );

			break;

		case APP_CMD_PAUSE:
			pEngine->SetPaused( true );

			break;
	}
}

void AndroidTask::Register()
{
	app_dummy();
}

