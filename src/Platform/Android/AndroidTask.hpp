/*
 * AndroidTask.hpp
 *
 *  Created on: 14/nov/2014
 *      Author: Simone
 */

#ifndef SRC_PLATFORM_ANDROID_ANDROIDTASK_HPP_
#define SRC_PLATFORM_ANDROID_ANDROIDTASK_HPP_

#include "android_native_app_glue.h"
#include <assert.h>
#include "../SystemTask.hpp"

namespace MalpenEngine
{
	class Engine;

	class AndroidTask : public SystemTask
	{
		public:
			AndroidTask( void* data, const unsigned int priority, Engine* pEngine );
			~AndroidTask();

			virtual bool Start();
			virtual void OnSuspend();
			virtual void Update();
			virtual void OnResume();
			virtual void Stop();

			virtual void Register();

		private:

			static void android_handle_cmd( struct android_app* app, int32_t cmd );

			android_app* mPState;
	};
}


#endif /* SRC_PLATFORM_ANDROID_ANDROIDTASK_HPP_ */
