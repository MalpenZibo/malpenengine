/*
 * SystemTask.hpp
 *
 *  Created on: 14/nov/2014
 *      Author: Simone
 */

#ifndef SRC_PLATFORM_SYSTEMTASK_HPP_
#define SRC_PLATFORM_SYSTEMTASK_HPP_

#include "../Core/Task.hpp"
#include "../Enum/LogSeverity.hpp"

namespace MalpenEngine
{
	class SystemTask : public Task
	{
		public:
			SystemTask( void* data, const unsigned int priority );
			virtual ~SystemTask();

			virtual bool Start() = 0;
			virtual void OnSuspend() = 0;
			virtual void Update() = 0;
			virtual void OnResume() = 0;
			virtual void Stop() = 0;

			virtual void Register() = 0;
	};
}

#endif /* SRC_PLATFORM_SYSTEMTASK_HPP_ */
