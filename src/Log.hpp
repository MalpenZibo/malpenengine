#ifndef LOG_H
#define LOG_H

#include "Enum/LogSeverity.hpp"

namespace MalpenEngine
{
	class Log
	{
		public:
			static void Info( const char* logMsg, ... );

			static void Warn( const char* logMsg, ... );

			static void Error( const char* logMsg, ... );

		private:
			static void PerformLog( LogSeverity severity, const char* logMsg, ... );
	};
}

#endif /* LOG_H */
