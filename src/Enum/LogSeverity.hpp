#ifndef ENUM_LOGSEVERITY_H
#define ENUM_LOGSEVERITY_H

namespace MalpenEngine
{
	enum LogSeverity
	{
	   Info,
	   Warn,
	   Err
	};
}

#endif /* ENUM_LOGSEVERITY_H */
