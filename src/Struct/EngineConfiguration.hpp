#ifndef STRUCT_ENGINECONFIGURATION_H
#define STRUCT_ENGINECONFIGURATION_H

#include <Enum/RendersType.hpp>

namespace MalpenEngine
{
	struct EngineConfiguration
	{
		int framerate;
		RendersType render;
	};
}
#endif /* STRUCT_ENGINECONFIGURATION_H */
