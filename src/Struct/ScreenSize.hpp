#ifndef STRUCT_SCREEN_SIZE_H
#define STRUCT_SCREEN_SIZE_H

#include <stdint.h>

struct ScreenSize
{
	int32_t pixelWidth, pixelHeight;
	float width, height;
};

#endif
