#include "Log.hpp"
#include <stdarg.h>

#ifdef __ANDROID__
	#include <android/log.h>
#else
	#error OS System not specified
#endif

using namespace MalpenEngine;

void Log::Info( const char* logMsg, ... )
{
	va_list lVarArgs;

	va_start( lVarArgs, logMsg );

	PerformLog( LogSeverity::Info, logMsg, lVarArgs );

	va_end( lVarArgs );
}

void Log::Warn( const char* logMsg, ... )
{
	va_list lVarArgs;

	va_start( lVarArgs, logMsg );

	PerformLog( LogSeverity::Warn, logMsg, lVarArgs );

	va_end( lVarArgs );
}

void Log::Error( const char* logMsg, ... )
{
	va_list lVarArgs;

	va_start( lVarArgs, logMsg );

	PerformLog( LogSeverity::Err, logMsg, lVarArgs );

	va_end( lVarArgs );
}

void Log::PerformLog( LogSeverity severity, const char* logMsg, ... )
{
	va_list lVarArgs;

	va_start( lVarArgs, logMsg );

#ifdef __ANDROID__
	switch( severity )
	{
		case LogSeverity::Info:
			__android_log_print( ANDROID_LOG_INFO, "MalpenEngine", logMsg, lVarArgs );
			break;
		case LogSeverity::Warn:
			__android_log_print( ANDROID_LOG_WARN, "MalpenEngine", logMsg, lVarArgs );
			break;
		case LogSeverity::Err:
			__android_log_print( ANDROID_LOG_ERROR, "MalpenEngine", logMsg, lVarArgs );
			break;
	}
#else
	#error OS System not specified
#endif

	va_end( lVarArgs );
}
