/*
 * Kernel.hpp
 *
 *  Created on: 14/nov/2014
 *      Author: Simone
 */

#ifndef SRC_CORE_KERNEL_HPP_
#define SRC_CORE_KERNEL_HPP_

#include <list>
#include <algorithm>
#include "Task.hpp"

namespace MalpenEngine
{
	class Kernel
	{
		public:
			Kernel();
			~Kernel();

			void Execute();

			bool AddTask( Task* pTask );
			void SuspendTask( Task* task );
			void ResumeTask( Task* task );
			void RemoveTask( Task* task );
			void KillAllTask();

			bool HasTask();

		private:
			void PriorityAdd( Task* pTask );

			std::list<Task*> mTasks;
			std::list<Task*> mPausedTasks;
	};
}

#endif /* SRC_CORE_KERNEL_HPP_ */
