/*
 * Task.hpp
 *
 *  Created on: 14/nov/2014
 *      Author: Simone
 */

#ifndef SRC_CORE_TASK_HPP_
#define SRC_CORE_TASK_HPP_

namespace MalpenEngine
{
	class Task
	{
		public:
			explicit Task( const unsigned int priority );
			virtual ~Task();

			virtual bool Start() = 0;
			virtual void OnSuspend() = 0;
			virtual void Update() = 0;
			virtual void OnResume() = 0;
			virtual void Stop() = 0;

			void SetCanKill( const bool canKill );
			bool CanKill() const;
			unsigned int Priority() const;

			static const unsigned int	TIMER_PRIORITY		= 0;
			static const unsigned int	PLATFORM_PRIORITY	= 1000;
			static const unsigned int	FILE_PRIORITY		= 2000;
			static const unsigned int	GAME_PRIORITY		= 3000;
			static const unsigned int	AUDIO_PRIORITY		= 4000;
			static const unsigned int	RENDER_PRIORITY		= 5000;

		private:
			unsigned int mPriority;
			bool mCanKill;
	};
}

#endif /* SRC_CORE_TASK_HPP_ */
