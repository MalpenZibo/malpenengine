/*
 * Task.cpp
 *
 *  Created on: 17/nov/2014
 *      Author: Simone
 */

#include "Task.hpp"

using namespace MalpenEngine;

Task::Task( const unsigned int priority )
{
	mPriority = priority;
	mCanKill = false;
}

Task::~Task()
{
}

void Task::SetCanKill( const bool canKill )
{
	mCanKill = canKill;
}

bool Task::CanKill() const
{
	return mCanKill;
}

unsigned int Task::Priority() const
{
	return mPriority;
}


