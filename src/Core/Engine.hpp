#ifndef CORE_ENGINE_H
#define CORE_ENGINE_H

#include <stdexcept>
#include "Kernel.hpp"
#include "../Platform/SystemTask.hpp"
#include "../Helpers/Timer.hpp"

#ifdef __ANDROID__
	#include "../Platform/Android/AndroidTask.hpp"
#else
	#error OS System not specified
#endif

namespace MalpenEngine
{
	class RendererTask;
	class Renderable;

	class Engine
	{
		public:
			Engine( void* data );

			void Register();

			bool Initialize();
			void Run();
			void StartRender();
			void StopRender();
			void Stop();
			void SetPaused( bool paused );

			void AddTask( Task* pTask );
			RendererTask* GetRenderer() 	{ return mRendererTask; }

		private:

			void* systemData;

			bool mClosing;
			bool mPaused;
			Kernel mKernel;

			SystemTask* mSystemTask;
			Timer mTimerTask;
			RendererTask* mRendererTask;

	};
}

extern void MalpenEngineAppMain( MalpenEngine::Engine* engine );

#endif /* CORE_ENGINE_H */
