/*
 * Kernel.cpp
 *
 *  Created on: 14/nov/2014
 *      Author: Simone
 */

#include "Kernel.hpp"
#include "../Platform/SystemTask.hpp"

using namespace MalpenEngine;

Kernel::Kernel()
{
}

Kernel::~Kernel()
{
}

void Kernel::PriorityAdd( Task* pTask )
{
	std::list<Task*>::iterator iter;
	for( iter = mTasks.begin(); iter != mTasks.end(); ++iter )
	{
		Task* pCurrentTask = (*iter);
		if( pCurrentTask->Priority() > pTask->Priority() )
		{
			break;
		}
	}

	mTasks.insert( iter, pTask );
}

bool Kernel::AddTask( Task* pTask )
{
	bool started = pTask->Start();

	if( started )
	{
		PriorityAdd( pTask );
	}
	return started;
}

void Kernel::RemoveTask( Task* pTask )
{
	if( std::find( mTasks.begin(), mTasks.end(), pTask ) != mTasks.end() )
	{
		pTask->SetCanKill( true );
	}
}

void Kernel::SuspendTask( Task* pTask )
{
	if( std::find( mTasks.begin(), mTasks.end(), pTask ) != mTasks.end() )
	{
		pTask->OnSuspend();
		mTasks.remove( pTask );
		mPausedTasks.push_back( pTask );
	}
}

void Kernel::ResumeTask( Task* pTask )
{
	if( std::find( mPausedTasks.begin(), mPausedTasks.end(), pTask ) != mPausedTasks.end() )
	{
		pTask->OnResume();
		mPausedTasks.remove( pTask );

		PriorityAdd( pTask );
	}
}

void Kernel::KillAllTask()
{
	for( std::list<Task*>::iterator iter = mTasks.begin(); iter != mTasks.end(); ++iter )
	{
		(*iter)->SetCanKill( true );
	}
}

void Kernel::Execute()
{
	while( mTasks.size() )
	{
		std::list<Task*>::iterator iter;

		for( std::list<Task*>::iterator iter = mTasks.begin(); iter != mTasks.end(); ++iter )
		{
			Task* pTask = (*iter);
			if( !pTask->CanKill() )
			{
				pTask->Update();
			}
		}

		for( std::list<Task*>::iterator iter = mTasks.begin(); iter != mTasks.end(); )
		{
			Task* pTask = (*iter);
			++iter;
			if( pTask->CanKill() )
			{
				pTask->Stop();
				mTasks.remove( pTask );
				pTask = nullptr;
			}
		}
	}
}

bool Kernel::HasTask()
{
	return mTasks.size();
}



