#include "Engine.hpp"
#include "../Renders/OpenGLES/OpenGLES2.hpp"
#include "../Log.hpp"

using namespace MalpenEngine;

Engine::Engine( void* data )
	: mTimerTask( Task::TIMER_PRIORITY )
	, mRendererTask( nullptr )
	, mPaused( false )
	, mClosing( false )
	, systemData( data )
	, mSystemTask( nullptr )
{
}

bool Engine::Initialize()
{
	mClosing = false;

#ifdef __ANDROID__
	mSystemTask = new AndroidTask( systemData, Task::PLATFORM_PRIORITY, this );
#else
	#error OS System not specified
#endif

	mRendererTask = new OpenGLES2();

	Log::Info( "Initialized System Task And RenderTask" );

	MalpenEngineAppMain( this );

	//mKernel.AddTask( mSystemTask.get() );
	mKernel.AddTask( &mTimerTask );
	mKernel.AddTask( mSystemTask );

	return true;
}

void Engine::Run()
{
	mKernel.Execute();
}

void Engine::StartRender()
{
	void* window = nullptr;

	window = ((android_app*)systemData)->window;

	mRendererTask->SetWindow( window );

	mKernel.AddTask( mRendererTask );
}

void Engine::StopRender()
{
	mKernel.RemoveTask( mRendererTask );
}

void Engine::Stop()
{
	mKernel.KillAllTask();
}

void Engine::Register()
{
	mSystemTask->Register();
}

void Engine::AddTask( Task* pTask )
{
	mKernel.AddTask( pTask );
}

void Engine::SetPaused( bool paused )
{
	mPaused = paused;
}
