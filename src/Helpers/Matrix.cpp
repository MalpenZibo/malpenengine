#include <Helpers/Matrix.hpp>
#include "../Log.hpp"

float degreesToRadians( float degrees )
{
	return degrees * (M_PI / 180);
};

const void Matrix::Ortogonal( float* matrix, float left, float right, float bottom, float top, float near, float far )
{
	float r_l = right - left;
	float t_b = top - bottom;
	float f_n = far - near;
	float tx = - (right + left) / (right - left);
	float ty = - (top + bottom) / (top - bottom);
	float tz = - (far + near) / (far - near);

	matrix[0] = 2.0f / r_l;
	matrix[1] = 0.0f;
	matrix[2] = 0.0f;
	matrix[3] = tx;

	matrix[4] = 0.0f;
	matrix[5] = 2.0f / t_b;
	matrix[6] = 0.0f;
	matrix[7] = ty;

	matrix[8] = 0.0f;
	matrix[9] = 0.0f;
	matrix[10] = 2.0f / f_n;
	matrix[11] = tz;

	matrix[12] = 0.0f;
	matrix[13] = 0.0f;
	matrix[14] = 0.0f;
	matrix[15] = 1.0f;

}

const void Matrix::SetLookAtM( float* rm, int rmOffset, float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ )
{
	// See the OpenGL GLUT documentation for gluLookAt for a description
	// of the algorithm. We implement it in a straightforward way:

	float fx = centerX - eyeX;
	float fy = centerY - eyeY;
	float fz = centerZ - eyeZ;

	// Normalize f
	float rlf = 1.0f / Length( fx, fy, fz );
	fx *= rlf;
	fy *= rlf;
	fz *= rlf;

	// compute s = f x up (x means "cross product")
	float sx = fy * upZ - fz * upY;
	float sy = fz * upX - fx * upZ;
	float sz = fx * upY - fy * upX;

	// and normalize s
	float rls = 1.0f / Length( sx, sy, sz );
	sx *= rls;
	sy *= rls;
	sz *= rls;

	// compute u = s x f
	float ux = sy * fz - sz * fy;
	float uy = sz * fx - sx * fz;
	float uz = sx * fy - sy * fx;

	rm[rmOffset + 0] = sx;
	rm[rmOffset + 1] = ux;
	rm[rmOffset + 2] = -fx;
	rm[rmOffset + 3] = 0.0f;

	rm[rmOffset + 4] = sy;
	rm[rmOffset + 5] = uy;
	rm[rmOffset + 6] = -fy;
	rm[rmOffset + 7] = 0.0f;

	rm[rmOffset + 8] = sz;
	rm[rmOffset + 9] = uz;
	rm[rmOffset + 10] = -fz;
	rm[rmOffset + 11] = 0.0f;

	rm[rmOffset + 12] = 0.0f;
	rm[rmOffset + 13] = 0.0f;
	rm[rmOffset + 14] = 0.0f;
	rm[rmOffset + 15] = 1.0f;

	TranslateM( rm, rmOffset, -eyeX, -eyeY, -eyeZ );
}

const void Matrix::TranslateM( float* m, int mOffset, float x, float y, float z )
{
	for( int i = 0; i < 4; i++ )
	{
		int mi = mOffset + i;
		m[12 + mi] += m[mi] * x + m[4 + mi] * y + m[8 + mi] * z;
	}
}

const float Matrix::Length( float x, float y, float z )
{
	return (float)sqrt( x * x + y * y + z * z );
}

const void Matrix::Identity( float* identity )
{
	identity[0] = identity[5] = identity[10] = identity[15] = 1.0f;
	identity[1] = identity[2] = identity[3] = identity[4] = 0.0f;
	identity[6] = identity[7] = identity[8] = identity[9] = 0.0f;
	identity[11] = identity[12] = identity[13] = identity[14] = 0.0f;
}
const void Matrix::Translate( float x, float y, float z, float* matrix )
{
	Matrix::Identity( matrix );

	//translate slots
	matrix[12] = x;
	matrix[13] = y;
	matrix[14] = z;
}
const void Matrix::Scale( float sx, float sy, float sz, float* matrix )
{
	Matrix::Identity( matrix );

	//scale slots
	matrix[0] = sx;
	matrix[5] = sy;
	matrix[10] = sz;
}
const void Matrix::Rotate( float degrees, float* matrix )
{
	float radians = degreesToRadians(degrees);

	Matrix::Identity(matrix);

	// Rotate X formula.
	matrix[5] = cosf(radians);
	matrix[6] = sinf(radians);
	matrix[9] = -matrix[6];
	matrix[10] = matrix[5];
}

const void Matrix::RotateX( float degrees, float* matrix )
{
	float radians = degreesToRadians(degrees);

	Matrix::Identity(matrix);

	// Rotate X formula.
	matrix[5] = cosf(radians);
	matrix[6] = -sinf(radians);
	matrix[9] = -matrix[6];
	matrix[10] = matrix[5];
}
const void Matrix::RotateY( float degrees, float* matrix )
{
	float radians = degreesToRadians(degrees);

	Matrix::Identity(matrix);

	// Rotate X formula.
	matrix[5] = cosf(radians);
	matrix[6] = -sinf(radians);
	matrix[9] = -matrix[6];
	matrix[10] = matrix[5];
}
const void Matrix::RotateZ( float degrees, float* matrix )
{
	float radians = degreesToRadians(degrees);

	Matrix::Identity(matrix);

	// Rotate X formula.
	matrix[0] = cosf(radians);
	matrix[5] = matrix[0];
	matrix[1] = sinf(radians);
	matrix[4] = -matrix[1];
}

const void Matrix::Multiply( float* m1, float* m2, float* result )
{
    // Fisrt Column
    result[0] = m1[0]*m2[0] + m1[4]*m2[1] + m1[8]*m2[2] + m1[12]*m2[3];
    result[1] = m1[1]*m2[0] + m1[5]*m2[1] + m1[9]*m2[2] + m1[13]*m2[3];
    result[2] = m1[2]*m2[0] + m1[6]*m2[1] + m1[10]*m2[2] + m1[14]*m2[3];
    result[3] = m1[3]*m2[0] + m1[7]*m2[1] + m1[11]*m2[2] + m1[15]*m2[3];

    // Second Column
    result[4] = m1[0]*m2[4] + m1[4]*m2[5] + m1[8]*m2[6] + m1[12]*m2[7];
    result[5] = m1[1]*m2[4] + m1[5]*m2[5] + m1[9]*m2[6] + m1[13]*m2[7];
    result[6] = m1[2]*m2[4] + m1[6]*m2[5] + m1[10]*m2[6] + m1[14]*m2[7];
    result[7] = m1[3]*m2[4] + m1[7]*m2[5] + m1[11]*m2[6] + m1[15]*m2[7];

    // Third Column
    result[8] = m1[0]*m2[8] + m1[4]*m2[9] + m1[8]*m2[10] + m1[12]*m2[11];
    result[9] = m1[1]*m2[8] + m1[5]*m2[9] + m1[9]*m2[10] + m1[13]*m2[11];
    result[10] = m1[2]*m2[8] + m1[6]*m2[9] + m1[10]*m2[10] + m1[14]*m2[11];
    result[11] = m1[3]*m2[8] + m1[7]*m2[9] + m1[11]*m2[10] + m1[15]*m2[11];

    // Fourth Column
    result[12] = m1[0]*m2[12] + m1[4]*m2[13] + m1[8]*m2[14] + m1[12]*m2[15];
    result[13] = m1[1]*m2[12] + m1[5]*m2[13] + m1[9]*m2[14] + m1[13]*m2[15];
    result[14] = m1[2]*m2[12] + m1[6]*m2[13] + m1[10]*m2[14] + m1[14]*m2[15];
    result[15] = m1[3]*m2[12] + m1[7]*m2[13] + m1[11]*m2[14] + m1[15]*m2[15];
}
