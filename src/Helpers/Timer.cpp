/*
 * Timer.cpp
 *
 *  Created on: 17/nov/2014
 *      Author: Simone
 */

#include "Timer.hpp"
#include <time.h>

using namespace MalpenEngine;

Timer::Timer( unsigned int priority )
	: Task( priority )
	, mTimeLastFrame( 0 )
	, mFrameDt( 0.0f )
	, mSimDt( 0.0f )
	, mSimMultiplier( 1.0f )
{
}

Timer::~Timer()
	{
	}

Timer::TimeUnits Timer::nanoTime()
{
	timespec now;
	int err = clock_gettime( CLOCK_MONOTONIC, &now );
	return now.tv_sec * 1000000000LL + now.tv_nsec;
}

bool Timer::Start()
{
	mTimeLastFrame = nanoTime();
	return true;
}

void Timer::OnSuspend()
{
}

void Timer::Update()
{
	// Get the delta between the last frame and this
	TimeUnits currentTime = nanoTime();
	const float MULTIPLIER = 0.000000001f;
	mFrameDt = ( currentTime - mTimeLastFrame ) * MULTIPLIER;
	mTimeLastFrame = currentTime;

	mSimDt = mFrameDt * mSimMultiplier;
}

void Timer::OnResume()
{
	mTimeLastFrame = nanoTime();
}

void Timer::Stop()
{
}


