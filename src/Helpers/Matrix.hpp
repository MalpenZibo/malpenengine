#ifndef HELPERS_MATRIX_H
#define HELPERS_MATRIX_H

#include <math.h>

class Matrix
{
	public:
		static const void Ortogonal( float* matrix, float left, float right, float bottom, float top, float near, float far );

		static const void SetLookAtM( float* rm, int rmOffset, float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ );
		static const void TranslateM( float* m, int mOffset, float x, float y, float z );
		static const float Length( float x, float y, float z );

		static const void Identity( float* identity );
		static const void Translate( float x, float y, float z, float* matrix );
		static const void Scale( float sx, float sy, float sz, float* matrix );
		static const void Rotate( float degrees, float* matrix );
		static const void RotateX( float degrees, float* matrix );
		static const void RotateY( float degrees, float* matrix );
		static const void RotateZ( float degrees, float* matrix );
		static const void Multiply( float* m1, float* m2, float* result );
};


#endif
