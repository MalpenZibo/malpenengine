/*
 * Timer.hpp
 *
 *  Created on: 17/nov/2014
 *      Author: Simone
 */

#ifndef SRC_HELPERS_TIMER_HPP_
#define SRC_HELPERS_TIMER_HPP_

#include "../Core/Task.hpp"

namespace MalpenEngine
{
	class Timer : public Task
	{
		public:
			typedef long long TimeUnits;

			Timer( const unsigned int priority );
			~Timer();

			float GetTimeFrame() const;
			float GetTimeSim() const;
			void SetSimMultiplier( const float simMultiplier );

			virtual bool Start();
			virtual void OnSuspend();
			virtual void Update();
			virtual void OnResume();
			virtual void Stop();

		private:
			TimeUnits nanoTime();

			TimeUnits mTimeLastFrame;
			TimeUnits mFrameDt;
			TimeUnits mSimDt;
			TimeUnits mSimMultiplier;
	};
}

#endif /* SRC_HELPERS_TIMER_HPP_ */
