/*
 * RenderTask.hpp
 *
 *  Created on: 17/nov/2014
 *      Author: Simone
 */

#ifndef SRC_RENDERS_RENDERERTASK_HPP_
#define SRC_RENDERS_RENDERERTASK_HPP_

#include "../Core/Task.hpp"
#include <vector>

namespace MalpenEngine
{
	class Shader;
	class Texture;
	class Renderable;

	class RendererTask : public Task
	{
		public:
			RendererTask( const unsigned int priority );
			virtual ~RendererTask();

			void SetWindow( void* window );

			virtual bool Start() = 0;
			virtual void OnSuspend() = 0;
			virtual void Update() = 0;
			virtual void OnResume() = 0;
			virtual void Stop() = 0;

			void AddRenderable( Renderable* pRenderable );
			void RemoveRenderable( Renderable* pRenderable );

			void AddShader( Shader* pShader );

		protected:
			virtual void Draw( Renderable* pRenderable ) = 0;
			std::vector<Renderable*> mRenderables;
			std::vector<Shader*> mShaders;

			void* mPWindow;

	};
}

#endif /* SRC_RENDERS_RENDERERTASK_HPP_ */
