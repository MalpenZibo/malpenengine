/*
 * RenderTask.cpp
 *
 *  Created on: 17/nov/2014
 *      Author: Simone
 */

#include <Renders/RendererTask.hpp>

namespace MalpenEngine
{

	RendererTask::RendererTask( unsigned int priority )
		: Task( priority )
		, mPWindow( nullptr )
	{
	}

	RendererTask::~RendererTask()
	{
	}

	void RendererTask::AddRenderable( Renderable* pRenderable )
	{
		mRenderables.push_back( pRenderable );
	}

	void RendererTask::RemoveRenderable( Renderable* pRenderable )
	{
		for(
			std::vector<Renderable*>::iterator iter = mRenderables.begin();
			iter != mRenderables.end();
			++iter
		)
		{
			Renderable* pCurrent = *iter;
			if( pCurrent == pRenderable )
			{
				mRenderables.erase( iter );
				break;
			}
		}
	}

	void RendererTask::AddShader( Shader* pShader )
	{
		mShaders.push_back( pShader );
	}

	void RendererTask::SetWindow( void* pWindow )
	{
		mPWindow = pWindow;
	}
}


