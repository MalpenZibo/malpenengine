/*
 * Geometry.hpp
 *
 *  Created on: 19/nov/2014
 *      Author: Simone
 */

#ifndef SRC_RENDERS_GEOMETRY_HPP_
#define SRC_RENDERS_GEOMETRY_HPP_

#include <string>

namespace MalpenEngine
{
	class Geometry
	{
		public:
			Geometry();
			virtual ~Geometry();

			void SetName( const char* name ) 				{ strcpy( mName, name ); }
			void SetNumVertices( const int numVertices )	{ mNumVertices = numVertices; }
			void SetNumIndices( const int numIndices )		{ mNumIndices = numIndices; }

			const char* GetName()							{ return mName; }

			const int GetNumVertices() const				{ return mNumVertices; }
			const int GetNumIndices() const					{ return mNumIndices; }

			void* GetVertexBuffer() const					{ return mPVertices; }
			void* GetIndexBuffer() const					{ return mPIndices; }

			void SetVertexBuffer( void* pVertices )			{ mPVertices = pVertices; }
			void SetIndexBuffer( void* pIndices )			{ mPIndices = pIndices; }

			void SetNumVertexPositionElements( const int numVertexPositionElements );
			int	GetNumVertexPositionElements() const		{ return mNumVertexPositionElements; }

			void SetNumColorElements( const int numColorElements );
			int GetNumColorElements() const					{ return mNumColorElements; }

			void SetNumTexCoordElements( const int numTexCoordElements );
			int	GetNumTexCoordElements() const				{ return mNumTexCoordElements; }

			void SetVertexStride(const int vertexStride)	{ mVertexStride = vertexStride; }
			int	GetVertexStride() const						{ return mVertexStride; }

		private:
			static const unsigned int NAME_MAX_LENGHT = 16;

			char 	mName[NAME_MAX_LENGHT];
			int 	mNumVertices;
			int 	mNumIndices;
			void* 	mPVertices;
			void* 	mPIndices;

			int 	mNumVertexPositionElements;
			int		mNumColorElements;
			int 	mNumTexCoordElements;
			int 	mVertexStride;
	};

	inline void Geometry::SetNumVertexPositionElements( const int numVertexPositionElements )
	{
		mNumVertexPositionElements = numVertexPositionElements;
	}

	inline void Geometry::SetNumColorElements( const int numColorElements )
	{
		mNumColorElements = numColorElements;
	}

	inline void Geometry::SetNumTexCoordElements( const int numTexCoordElements )
	{
		mNumTexCoordElements = numTexCoordElements;
	}
}

#endif /* SRC_RENDERS_GEOMETRY_HPP_ */
