#ifndef RENDERS_IRENDER_H
#define RENDERS_IRENDER_H

#include <vector>

namespace MalpenEngine
{
	class IRender
	{
		public:
			virtual ~IRender() {};

			virtual void InitializeRender() = 0;
			virtual void DestroyRender() = 0;
			virtual int DrawScene() = 0;
	};
}

#endif
