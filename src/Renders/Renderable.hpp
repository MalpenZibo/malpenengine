/*
 * Renderable.hpp
 *
 *  Created on: 19/nov/2014
 *      Author: Simone
 */

#ifndef SRC_RENDERS_RENDERABLE_HPP_
#define SRC_RENDERS_RENDERABLE_HPP_

namespace MalpenEngine
{
	class Geometry;
	class Shader;

	class Renderable
	{
		public:
			Renderable();
			~Renderable();

			void SetGeometry( Geometry* pGeometry );
			Geometry* GetGeometry();

			void SetShader( Shader* pShader );
			Shader* GetShader();

		private:
			Geometry*	mPGeometry;
			Shader*		mPShader;
	};

	inline Renderable::Renderable()
		:	mPGeometry( nullptr )
		,	mPShader( nullptr )
	{

	}

	inline Renderable::~Renderable()
	{

	}

	inline void Renderable::SetGeometry( Geometry* pGeometry )
	{
		mPGeometry = pGeometry;
	}

	inline Geometry* Renderable::GetGeometry()
	{
		return mPGeometry;
	}

	inline void Renderable::SetShader( Shader* pShader )
	{
		mPShader = pShader;
	}

	inline Shader* Renderable::GetShader()
	{
		return mPShader;
	}
}

#endif /* SRC_RENDERS_RENDERABLE_HPP_ */
