/*
 * Geometry.cpp
 *
 *  Created on: 19/nov/2014
 *      Author: Simone
 */

#include "Geometry.hpp"

namespace MalpenEngine
{
	Geometry::Geometry()
		:	mNumVertices( 0 )
		,	mNumIndices( 0 )
		,	mPVertices( nullptr )
		,	mPIndices( nullptr )
		,	mNumVertexPositionElements( 0 )
		,	mNumTexCoordElements( 0 )
		,	mVertexStride( 0 )
		,	mNumColorElements( 0 )
	{
	}

	Geometry::~Geometry()
	{
	}
}


