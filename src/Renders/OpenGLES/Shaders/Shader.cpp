/*
 * Shader.cpp
 *
 *  Created on: 17/nov/2014
 *      Author: Simone
 */

#include "Shader.hpp"

using namespace MalpenEngine;

Shader::Shader()
	: mVertexShaderId( GL_INVALID_VALUE )
	, mFragmentShaderId( GL_INVALID_VALUE )
	, mProgramId( GL_INVALID_VALUE )
	, mIsLinked( false )
{
}

Shader::~Shader()
{

}

void Shader::LoadShader( GLuint id, std::string& shaderCode )
{
	static const uint32_t NUM_SHADERS = 1;

	const GLchar* pCode = shaderCode.c_str();
	GLint length = shaderCode.length();

	glShaderSource( id, NUM_SHADERS, &pCode, &length );

	glCompileShader( id );

	glAttachShader( mProgramId, id );
}

void Shader::Link()
{
	mProgramId = glCreateProgram();

	mVertexShaderId = glCreateShader( GL_VERTEX_SHADER );
	LoadShader( mVertexShaderId, mVertexShaderCode );

	mFragmentShaderId = glCreateShader( GL_FRAGMENT_SHADER );
	LoadShader( mFragmentShaderId, mFragmentShaderCode );

	glLinkProgram( mProgramId );

	mIsLinked = true;
}

void Shader::Setup( Renderable& renderable )
{
	glUseProgram( mProgramId );
}

bool Shader::IsLinked()
{
	return mIsLinked;
}



