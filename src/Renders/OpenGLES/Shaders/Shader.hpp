/*
 * Scader.hpp
 *
 *  Created on: 17/nov/2014
 *      Author: Simone
 */

#ifndef SRC_RENDERS_OPENGLES_SHADERS_SHADER_HPP_
#define SRC_RENDERS_OPENGLES_SHADERS_SHADER_HPP_

#include <GLES2/gl2.h>
#include <string>

namespace MalpenEngine
{
	class Renderable;

	class Shader
	{
		public:
			Shader();
			virtual ~Shader();

			virtual void Link();
			virtual void Setup( Renderable& renderable );

			bool IsLinked();

		protected:
			unsigned int mVertexShaderId;
			unsigned int mFragmentShaderId;
			unsigned int mProgramId;

			std::string mVertexShaderCode;
			std::string mFragmentShaderCode;

			bool mIsLinked;

		private:
			void LoadShader( unsigned int shaderType, std::string& shaderCode );
	};
}

#endif /* SRC_RENDERS_OPENGLES_SHADERS_SHADER_HPP_ */
