/*
 * BasicShader.hpp
 *
 *  Created on: 19/nov/2014
 *      Author: Simone
 */

#ifndef SRC_RENDERS_OPENGLES_SHADERS_BASICSHADER_HPP_
#define SRC_RENDERS_OPENGLES_SHADERS_BASICSHADER_HPP_

#include "Shader.hpp"

namespace MalpenEngine
{
	class Renderable;

	class BasicShader : public Shader
	{
		public:
			BasicShader();
			virtual ~BasicShader();

			virtual void Link();
			virtual void Setup( Renderable& renderable );

		private:
			GLint mPositionAttributeHandle;
	};
}

#endif /* SRC_RENDERS_OPENGLES_SHADERS_BASICSHADER_HPP_ */
