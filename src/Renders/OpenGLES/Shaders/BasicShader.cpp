/*
 * BasicShader.cpp
 *
 *  Created on: 19/nov/2014
 *      Author: Simone
 */

#include "BasicShader.hpp"
#include <cassert>
#include "../../Geometry.hpp"
#include "../../Renderable.hpp"

namespace MalpenEngine
{
	BasicShader::BasicShader()
		: mPositionAttributeHandle( 0 )
	{
		mVertexShaderCode =
				"attribute vec4 a_vPosition;        \n"
				"void main(){                       \n"
				"     gl_Position = a_vPosition;	\n"
				"}                                  \n";

		mFragmentShaderCode =
				"precision mediump float;  						\n"
				"void main(){              						\n"
				"    gl_FragColor = vec4(0.2, 0.2, 0.2, 1.0);   \n"
				"}                         						\n";
	}

	BasicShader::~BasicShader()
	{

	}

	void BasicShader::Link()
	{
		Shader::Link();

		mPositionAttributeHandle = glGetAttribLocation( mProgramId, "a_vPosition" );
	}

	void BasicShader::Setup( Renderable& renderable )
	{
		Shader::Setup( renderable );

		Geometry* pGeometry = renderable.GetGeometry();
		assert( pGeometry );

		glVertexAttribPointer(
				mPositionAttributeHandle,
				pGeometry->GetNumVertexPositionElements(),
				GL_FLOAT,
				GL_FALSE,
				pGeometry->GetVertexStride(),
				pGeometry->GetVertexBuffer()
		);

		glEnableVertexAttribArray( mPositionAttributeHandle );
	}
}


