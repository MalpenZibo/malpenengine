#ifndef RENDERS_OPENGLES2_H
#define RENDERS_OPENGLES2_H

#include <stdlib.h>
#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <Renders/RendererTask.hpp>

#include "Helpers/Matrix.hpp"
#include <stdint.h>
#include "Struct/ScreenSize.hpp"

namespace MalpenEngine
{
	class OpenGLES2 : public RendererTask
	{
		public:
			OpenGLES2();

			virtual bool Start();
			virtual void OnSuspend();
			virtual void Update();
			virtual void OnResume();
			virtual void Stop();

			void Init();
			void Destroy();
			//int DrawScene( Scene* target );
			//int DrawScene( std::vector<GraphicElements::StandardElement> elements );

		protected:
			virtual void Draw( Renderable* pRenderable );

		private:

			bool mIsInitialized;

			//EGLNativeWindowType mWindow;
			EGLDisplay mDisplay;
			EGLSurface mSurface;
			EGLContext mContext;

			int32_t mWidth, mHeight;

			/*
			GLuint CreateProgram( const char* pVertexSource, const char* pFragmentSource );
			GLuint LoadShader( GLenum shaderType, const char* pSource );

			static const char gVertexShader[];
			static const char gFragmentShader[];

			static float mVMatrix[16];
			static float mMVPMatrix[16];

			EGLNativeWindowType mWindow;
			EGLDisplay mDisplay;
			EGLSurface mSurface;
			EGLContext mContext;

			GLuint gProgram;

			int32_t mWidth, mHeight;
			float width, height, ratio;
			float lClearColor;

			ScreenSize screen;

			GLuint gvPositionHandle;
			GLuint mMVPMatrixHandle;
			GLuint mColorHandle;
			GLuint mTextureHandle;
			GLuint mTextureUniformHandle;
			GLuint mTextureFlag;
			float mProjMatrix[16];
			float tmpM1[16];
			float tmpM2[16];
			float tmpM3[16];*/
	};
}

#endif
