#include "OpenGLES2.hpp"
#include "../../Log.hpp"
#include "../Geometry.hpp"
#include "Shaders/Shader.hpp"
#include "../Renderable.hpp"

#include <assert.h>

namespace MalpenEngine
{
	OpenGLES2::OpenGLES2()
		: RendererTask( Task::RENDER_PRIORITY )
		, mIsInitialized( false )
		, mDisplay( nullptr )
		, mSurface( nullptr )
		, mContext( nullptr )
		, mWidth( 0.0f )
		, mHeight( 0.0f )
	{
	}

	bool OpenGLES2::Start()
	{
		Init();

		return true;
	}

	void OpenGLES2::OnSuspend()
	{
	}

	void OpenGLES2::Update()
	{
		if( mIsInitialized )
		{
			glClearColor( 0.95f, 0.95f, 0.95f, 1 );
			glClear( GL_COLOR_BUFFER_BIT );

			for(
				std::vector<Renderable*>::iterator iter = mRenderables.begin();
				iter != mRenderables.end();
				++iter
			)
			{
				Renderable* pCurrent = *iter;
				if( pCurrent )
				{
					Draw( pCurrent );
				}
			}

			eglSwapBuffers( mDisplay, mSurface );
		}
	}

	void OpenGLES2::OnResume()
	{
	}

	void OpenGLES2::Stop()
	{
		Destroy();
	}

	void OpenGLES2::Init()
	{
		Log::Info( "start render initialization" );

		EGLint lFormat, lNumConfigs, lErrorResult;
		EGLConfig lConfig;

		Log::Info( "set attribute" );
		const EGLint lAttributes[] =
		{
			EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
			EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
			EGL_BLUE_SIZE, 8,
			EGL_GREEN_SIZE, 8,
			EGL_RED_SIZE, 8,
			EGL_NONE
		};

		EGLint context_attribs[] = { EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE };

		Log::Info( "make display" );
		mDisplay = eglGetDisplay( EGL_DEFAULT_DISPLAY );

		if( mDisplay == EGL_NO_DISPLAY ) goto ERROR;

		Log::Info( "EGL initialize" );
		if( !eglInitialize( mDisplay, 0, 0 ) ) goto ERROR;

		Log::Info( "EGL choose config" );
		if( !eglChooseConfig( mDisplay, lAttributes, &lConfig, 1, &lNumConfigs ) || ( lNumConfigs <= 0 ) ) goto ERROR;

		Log::Info( "EGL get config attribute" );
		if( !eglGetConfigAttrib( mDisplay, lConfig, EGL_NATIVE_VISUAL_ID, &lFormat ) ) goto ERROR;

		Log::Info( "set buffer geometry" );

		ANativeWindow_setBuffersGeometry( (EGLNativeWindowType)mPWindow, 0, 0, lFormat );

		Log::Info( "EGL create windows surface" );
		mSurface = eglCreateWindowSurface( mDisplay, lConfig, (EGLNativeWindowType)mPWindow, nullptr );

		if( mSurface == EGL_NO_SURFACE ) goto ERROR;

		Log::Info( "EGL create context" );
		mContext = eglCreateContext( mDisplay, lConfig, EGL_NO_CONTEXT, context_attribs );

		if( mContext == EGL_NO_CONTEXT ) goto ERROR;

		Log::Info( "EGL makecurrent" );
		if( !eglMakeCurrent( mDisplay, mSurface, mSurface, mContext ) ) goto ERROR;

		Log::Info( "EGL query surface for width" );
		if( !eglQuerySurface( mDisplay, mSurface, EGL_WIDTH, &mWidth ) ) goto ERROR;

		Log::Info( "EGL query surface for height" );
		if( !eglQuerySurface( mDisplay, mSurface, EGL_HEIGHT, &mHeight ) ) goto ERROR;

		Log::Info( "Check width and height initialization" );
		if( ( mWidth <= 0 ) || ( mHeight <= 0 ) ) goto ERROR;

		for( std::vector<Shader*>::iterator iter = mShaders.begin(); iter != mShaders.end(); ++iter )
		{
			Shader* pCurrent = *iter;
			pCurrent->Link();
		}

		mIsInitialized = true;

		return;

		/*
		ratio = (float)mWidth / (float)mHeight;
		width = ratio;
		height = 1.0f;

		screen.pixelWidth = mWidth;
		screen.pixelHeight = mHeight;
		screen.width = width;
		screen.height = height;

		Log::Info( "Create program" );
		gProgram = CreateProgram( gVertexShader, gFragmentShader );
		if( !gProgram ) goto ERROR;

		Log::Info( "Get Attribute Location" );

		gvPositionHandle = glGetAttribLocation( gProgram, "vPosition" );
		mTextureUniformHandle = glGetUniformLocation( gProgram, "sTexture");
		mTextureHandle = glGetAttribLocation( gProgram, "aTextureCoord" );
		mMVPMatrixHandle = glGetUniformLocation( gProgram, "uMVPMatrix" );
		mColorHandle = glGetUniformLocation( gProgram, "vColor" );
		mTextureFlag = glGetUniformLocation( gProgram, "textureFlag" );

		glViewport( 0, 0, mWidth, mHeight );

		Log::Info( "Render Start Complete" );

		glHint( GL_GENERATE_MIPMAP_HINT, GL_FASTEST );

		glPixelStorei( GL_UNPACK_ALIGNMENT, 1 ); // correction, should be 1

		glClearColor( lClearColor, lClearColor, lClearColor, 1.0f );

		glEnable( GL_CULL_FACE );
		glEnable( GL_DEPTH_TEST );
		glDepthFunc( GL_LEQUAL );

		glEnable( GL_ALPHA );

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glEnable( GL_TEXTURE_2D );			//Enable Texture Mapping ( NEW )

		Matrix::Ortogonal( mProjMatrix, -width, width, -height, height, 1.0f, -1.0f );

		Log::Info( "width= %d height = %d", mWidth, mHeight );

		Log::Info( "Finish Render Start" );

		return;
*/
		ERROR:
			Log::Error("Error while starting GraphicsService");
			Destroy();


	}

	void OpenGLES2::Destroy()
	{
		Log::Info( "Start destroy OpenGLES2 Context" );
		mIsInitialized = false;

		if( mDisplay != EGL_NO_DISPLAY )
		{
			eglMakeCurrent( mDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT );
			if( mContext != EGL_NO_CONTEXT )
			{
				eglDestroyContext( mDisplay, mContext );
			}

			if( mSurface != EGL_NO_SURFACE )
			{
				eglDestroySurface( mDisplay, mSurface );
			}
			eglTerminate( mDisplay );
		}

		mDisplay = EGL_NO_DISPLAY;
		mContext = EGL_NO_CONTEXT;
		mSurface = EGL_NO_SURFACE;

		Log::Info( "Start destroy OpenGLES2 finish" );
	}

	void OpenGLES2::Draw( Renderable* pRenderable )
	{
		assert( pRenderable );
		if( pRenderable )
		{
			Geometry* pGeometry = pRenderable->GetGeometry();
			Shader* pShader = pRenderable->GetShader();
			assert( pShader && pGeometry );

			pShader->Setup( *pRenderable );

			glDrawElements(
				GL_TRIANGLES,
				pGeometry->GetNumIndices(),
				GL_UNSIGNED_SHORT,
				pGeometry->GetIndexBuffer()
			);
		}
	}
}
