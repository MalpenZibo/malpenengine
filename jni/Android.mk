LOCAL_PATH := $(call my-dir)/../src

include $(CLEAR_VARS)

# rtti and exceptions features
LOCAL_CPP_FEATURES += exceptions
LOCAL_CPPFLAGS := -std=c++11

LOCAL_MODULE := MalpenEngine

LOCAL_SRC_FILES := \
	Core/Engine.cpp \
	Core/Kernel.cpp \
	Core/Task.cpp \
	Helpers/Matrix.cpp \
	Helpers/Timer.cpp \
	Platform/Android/Android.cpp \
	Platform/Android/AndroidTask.cpp \
	Platform/Android/android_native_app_glue.c \
	Platform/SystemTask.cpp \
	Renders/OpenGLES/Shaders/BasicShader.cpp \
	Renders/OpenGLES/Shaders/Shader.cpp \
	Renders/OpenGLES/OpenGLES2.cpp \
	Renders/Geometry.cpp \
	Renders/RendererTask.cpp \
	Log.cpp
	
include $(BUILD_STATIC_LIBRARY)